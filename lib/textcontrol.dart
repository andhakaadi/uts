import 'package:flutter/material.dart';

class Control extends StatelessWidget{
  final String nama;
  Control(this.nama);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Text('$nama',style: new TextStyle(fontSize: 33.0,color: Colors.white),),
      ),
    );
  }
}