import 'package:flutter/material.dart';
import './textuotput.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS MOBILE',
      home: Scaffold(
        backgroundColor: Colors.grey[500],
        appBar: AppBar(
          backgroundColor: Colors.orange,
          leading: new Icon(Icons.home),
          title: Text('UTS-MOBILE'),
          actions: <Widget>[
          new Icon(Icons.search),
          new Icon(Icons.more_vert),
        ],
        ),
        body: Output(),
      ),
    );
  }
}