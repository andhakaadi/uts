import 'package:flutter/material.dart';
import './textcontrol.dart';

class Output extends StatefulWidget {
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output> {
  String msg = 'Andhaka';

  void _changeText() {
    setState(() {
      if (msg.startsWith('An')) {
        msg = 'Adi';
      } else if (msg.startsWith('P')) {
        msg = 'Andhaka';
      } else {
        msg = 'Pangestu';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'NAMA SAYA : ',
            style: new TextStyle(fontSize: 27.0, color: Colors.white),
          ),
          Control(msg),
          RaisedButton(
            child: Text(
              "GANTI NAMA",
              style: new TextStyle(color: Colors.white),
            ),
            color: Colors.orange,
            onPressed: _changeText,
          ),
        ],
      ),
    );
  }
}
